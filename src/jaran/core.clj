(ns jaran.core
  (:require [clojure.string :as str]
            [clojure.core.match :refer [match]]))

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

(defn average
  [numbers]
  (/ (apply + numbers) (count numbers)))

(defn is-an-instance-of-list
  [something]
  (instance? java.util.List something))

(def person {:name "Tom Kierke"
             :city "Santa Cruz, BR"})

(do
  (println "jaran")
  (apply * [1 3 5 7]))

(let [a (inc (rand-int 6))
      b (inc (rand-int 6))]
  (println (format "You rolled a %s and a %s" a b))
  (+ a b))

(defn hypot
  [x y]
  (defn square
    [x]
    (* x x))
  (let [x2 (square x)
        y2 (square y)]
    (Math/sqrt (+ x2 y2))))

(def m {:a 5
        :b 6
        :c [6 7 5]
        :d {:e 10
            :f 11}
        "foo" 88
        42 false})

(def map-in-vector ["James" {:birthday (java.util.Date. 73 1 6)}])

(let [[name {bd :birthday}] map-in-vector]
  (str name " was born on " bd))

(def chas {:name "Chas"
           :age 31
           :location "Aceh"})

(let [{name :name age :age location :location} chas]
  (format "%s is %s years old and lives in %s" name age location))

(let [{:keys [name age location]} chas]
  (format "%s is %s years old and lives in %s" name age location))

(def brian {"name" "Brian"
            "age" 35
            "location" "Here, There, Everywhere"})

(let [{:strs [name age location]} brian]
  (format "%s is %s years old and lives in %s" name age location))

(def sam {'name "Brian"
          'age 35
          'location "Here, There, Everywhere"})
(let [{:syms [name age location]} sam]
  (format "%s is %s years old and lives in %s" name age location))

(def user-info ["robert" 2011 :name "rob" :city "boston"])

(let [[username account-year & extra-info] user-info
      {:keys [name city]} (apply hash-map extra-info)]
  (format "username %s (%s) is in %s at year %s" name
          username
          city
          account-year))

((fn [x]
   (+ 10 x)) 42)

(def strange-adder (fn adder-self-reference
                     ([x] (adder-self-reference x 1))
                     ([x y] (+ x y))))

(letfn [(odd? [n]
          (even? (dec n)))
        (even? [n]
               (or (zero? n)
                   (odd? (dec n))))]
  (odd? 11))

(defn concat-rest
  [x & rest]
  (apply str (butlast rest)))

(defn make-user
  [username & {:keys [email join-date]
               :or {join-date (java.util.Date.)}}]
  {:username username
   :join-date join-date
   :email email
   :exp-date (java.util.Date. (long (+ 2.592e9 (.getTime join-date))))})

(loop [x 6]
  (if (neg? x)
    x
    (recur (dec x))))

(defn countdown
  [x]
  (if (zero? x)
    :blastoff!
    (do (println x)
        (recur (dec x)))))

(defn embedded-repl
  []
  (print (str (ns-name *ns*) ">> "))
  (flush)
  (let [expr (read)
        value (eval expr)]
    (when (not= :quit value)
      (println value)
      (recur))))

(require 'clojure.string)

(clojure.string/lower-case "Kojur Tenan.")

(def only-strings (partial filter string?))

(defn negated-sum-str
  [& numbers]
  (str (- (apply + numbers))))

(defn camel->keyword
  [s]
  (->> (str/split s #"(?<=[a-z])(?=[A-Z])")
       (map str/lower-case)
       (interpose \-)
       str/join
       keyword))

(def camel-pairs->map (comp (partial apply hash-map)
                            (partial map-indexed (fn [i x]
                                                   (if (odd? i)
                                                     x
                                                     (camel->keyword x))))))

(defn print-logger
  [writer]
  #(binding [*out* writer]
     (println %)))

(def *out*-logger (print-logger *out*))

(def writer (java.io.StringWriter.))

(def retained-logger (print-logger writer))

(defn any-other-prime?
  [n]
  (cond (= n 1) false
        (= n 2) true
        (even? n) false
        :else (->> (range 3 (inc (Math/sqrt n)) 2)
                   (filter #(zero? (rem n %)))
                   empty?)))

;;(let [m-prime? (memoize any-other-prime?)]
;;  (time (m-prime? 1125899906842679))
;;  (time (m-prime? 1125899906842679)))

(defn swap-pairs
  [sequentials]
  (into (empty sequentials)
        (interleave (take-nth 2 (drop 1 sequentials))
                    (take-nth 2 sequentials))))

(defn map-map
  [f m]
  (into (empty m)
        (for [[k v] m]
          [k (f v)])))

(defn lazy-random-i-guess
  [limit]
  (lazy-seq
   (println "What's going on?")
   (cons
    (rand-int limit)
    (lazy-random-i-guess limit))))

(defn remove-vowel
  [toberemoved]
  (apply str (remove (set "aoeuiy")
                     toberemoved)))

(defn magnitude
  [x]
  (-> x Math/log10 Math/floor))

(defn compare-magnitude
  [a b]
  (- (magnitude a) (magnitude b)))

(defn interpolate
  [points]
  (let [results (into (sorted-map) (map vec points))]
    (fn [x]
      (let [[xa ya] (first (rsubseq results <= x))
            [xb yb] (first (rsubseq results >  x))]
        (if (and xa xa)
          (/ (+ (* ya (- xb x)) (* yb (- x xa)))
             (- xb xa))
          (or ya yb))))))

(filter :age [{:name "Tom" :age 42}
              {:gender :f :name "Suzanne"}
              {:name "Sara" :location "New York"}])

(filter (comp (partial <= 23) :age) [{:name "Tom" :age 42}
                                     {:gender :f :name "Suzanne" :age 46}
                                     {:name "Sara" :location "New York" :age 11}])

(defn euclid-division
  [x y]
  [(quot x y) (rem x y)])

(def playlist
  [{:title "Elephant" :artist "The White Stripes" :year 2003}
   {:title "Helioself" :artist "Papas Fritas" :year 1997}
   {:title "Stories from the City Stories from the Sea" :artist "PJ Harvey" :year 2000}
   {:title "Buildings and Grounds" :artist "Papas Fritas" :year 2000}
   {:title "Zen Rodeo" :artist "Mardi Gras BB" :year 2002}])

(defn summarise
  [{:keys [title artist year]}]
  (println title)
  (println artist)
  (println year)
  (str title " / " artist " / " year))

(def orders
  [{:product "Clock", :customer "Wile Coyote", :qty 6, :total 300}
   {:product "Dynamite", :customer "Wile Coyote", :qty 20, :total 5000}
   {:product "Shotgun", :customer "Elmer Fudd", :qty 2, :total 800}
   {:product "Shells", :customer "Elmer Fudd", :qty 4, :total 100}
   {:product "Hole", :customer "Wile Coyote", :qty 1, :total 1000}
   {:product "Anvil", :customer "Elmer Fudd", :qty 2, :total 300}
   {:product "Anvil", :customer "Wile Coyote", :qty 6, :total 900}])

(defn reduce-by
  [key-fn f init coll]
  (reduce (fn [summaries x]
            (let [k (key-fn x)]
              (assoc summaries k (f (summaries k init) x))))
          {} coll))

(reduce-by :customer #(+ %1 (:total %2)) 0 orders)

(reduce-by :product #(conj %1 (:customer %2)) #{} orders)

(defn reduce-by-in
  [keys-fn f init coll]
  (reduce (fn [summaries x]
            (let [ks (keys-fn x)]
              (assoc-in summaries ks
                        (f (get-in summaries ks init) x))))
          {} coll))

(reduce-by-in (juxt :customer :product) #(+ %1 (:total %2)) 0 orders)
